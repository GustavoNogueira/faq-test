from app import db

class Mensenger(db.Model):
    __tablename__ = "mensengers"

    id = db.Column(db.Integer , primary_key=True)
    name = db.Column(db.String(30))
    surname = db.Column(db.String(30))
    email = db.Column(db.String)
    text = db.Column(db.Text)
    subject = db.Column(db.Text,nullable=True)

    def __init__(self,name,surname,email,text,subject):
        self.name = name
        self.surname = surname
        self.email = email
        self.text = text
        self.subject = subject

    def __repr__(self):
        return "Nome: %r" % self.name + " %r " % self.surname + "email: %r " % self.email + "Assunto: %r " % self.subject + "Mensagem: %r " % self.text
