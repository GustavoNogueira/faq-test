from app import db

class Question(db.Model):
    __tablename__ = "perguntas"

    id = db.Column(db.Integer , primary_key=True)
    ordem = db.Column(db.Integer , unique=True, autoincrement=True)
    question = db.Column(db.Text)
    answer = db.Column(db.Text)

    def __init__(self,ordem,question,answer):
        self.ordem = ordem
        self.question = question
        self.answer = answer

    def __repr__(self):
        return "Question: %r" % self.question + " Resposta: %r " % self.answer
