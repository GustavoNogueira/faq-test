from flask import Flask, jsonify, request, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

app = Flask(__name__)
app.config.from_object('config')
#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///teste.db'
db = SQLAlchemy(app)

migrate = Migrate(app, db)

manager = Manager(app)

manager.add_command('db',MigrateCommand)

from app.models import questions, user
from app.models.questions import Question
from app.models.user import Mensenger

'''
#
#Função para inserção de mensagens
#
'''
@app.route("/cadastrar", methods=['POST'])
def cadastrar():
    if request.method == "POST":
        name = request.form.get("name")
        surname = request.form.get("surname")
        email = request.form.get("email")
        subject = request.form.get("subject")
        text = request.form.get("text")

        if name and surname and email and text:
            p = Mensenger(name,surname,email,subject,text)
            db.session.add(p)
            db.session.commit()

    return "OK"

'''
#
#Função para consulta de mensagens
#
'''

@app.route("/faq/<question>")
@app.route("/faq/",defaults={"question":None})
def listar(question):
    r=Question.query.order_by(Question.ordem).all()
    print(type(jsonify(list(map(str, r)))))
    return jsonify(list(map(str, r)))
