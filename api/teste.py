import os
import unittest

from app import app


class QuestionsEndpoint(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['DEBUG'] = False
        self.app = app.test_client()

    def test_endpoint_should_return_200(self):
        response = self.app.get('/faq')
        self.assertEqual(response.status_code, 200)

    def test_endpoint_post(self):
        response = self.app.post('/cadastrar')
        self.assertEqual("OK")


if __name__ == '__main__':
    unittest.main()
